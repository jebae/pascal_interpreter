#include <gtest/gtest.h>
#include "interpreter.hpp"
#include "ast.hpp"
#include "lexer.hpp"
#include "parser.hpp"
#include "setting.hpp"

class NodeVisitorVisitBinOpTest : public ::testing::Test
{
protected:
	NodeVisitor visitor;
	Num *left;
	Num *right;

	void SetUp() override
	{
		Token token1(_INTEGER, "42");
		Token token2(_INTEGER, "21");
		left = new Num(token1);
		right = new Num(token2);
	}

	void TearDown() override
	{
		delete left;
		delete right;
	}
};

TEST_F(NodeVisitorVisitBinOpTest, Plus)
{
	Token bin_token(_PLUS, "+");
	BinOp node(bin_token, left, right);
	EXPECT_EQ(42 + 21, visitor.visit(&node));
}

TEST_F(NodeVisitorVisitBinOpTest, Sub)
{
	Token bin_token(_SUB, "-");
	BinOp node(bin_token, left, right);
	EXPECT_EQ(42 - 21, visitor.visit(&node));
}

TEST_F(NodeVisitorVisitBinOpTest, Mul)
{
	Token bin_token(_MUL, "*");
	BinOp node(bin_token, left, right);
	EXPECT_EQ(42 * 21, visitor.visit(&node));
}

TEST_F(NodeVisitorVisitBinOpTest, Div)
{
	Token bin_token(_DIV, "/");
	BinOp node(bin_token, left, right);
	EXPECT_EQ(42 / 21, visitor.visit(&node));
}

TEST_F(NodeVisitorVisitBinOpTest, Invalid)
{
	Token bin_token(_BEGIN, "BEGIN");
	BinOp node(bin_token, left, right);

	try
	{
		visitor.visit(&node);
		FAIL();
	}
	catch (const char *err)
	{
		EXPECT_STREQ("Invalid node type", err);
	}
}

class NodeVisitorVisitUnaryOpTest : public ::testing::Test
{
protected:
	NodeVisitor visitor;
	Num *expr;

	void SetUp() override
	{
		Token token(_INTEGER, "42");
		expr = new Num(token);
	}

	void TearDown() override
	{
		delete expr;
	}
};

TEST_F(NodeVisitorVisitUnaryOpTest, Plus)
{
	Token unary_token(_PLUS, "+");
	UnaryOp node(unary_token, expr);

	EXPECT_EQ(42, visitor.visit(&node));
}

TEST_F(NodeVisitorVisitUnaryOpTest, Minus)
{
	Token unary_token(_SUB, "-");
	UnaryOp node(unary_token, expr);

	EXPECT_EQ(-42, visitor.visit(&node));
}

TEST_F(NodeVisitorVisitUnaryOpTest, Invalid)
{
	Token unary_token(_MUL, "*");
	UnaryOp node(unary_token, expr);

	try
	{
		visitor.visit(&node);
		FAIL();
	}
	catch (const char *err)
	{
		EXPECT_STREQ("Invalid node type", err);
	}
}

TEST(NodeVisitorVisitAssignTest, Valid)
{
	NodeVisitor visitor;
	Token id_token(_ID, "foo");
	Token num_token(_INTEGER, "42");
	Token assign_token(_ASSIGN, ":=");
	Var var_node(id_token);
	Num num_node(num_token);
	Assign node(assign_token, &var_node, &num_node);

	visitor.visit(&node);
	int *res = visitor.GLOBAL_SCOPE.get("foo");
	EXPECT_EQ(42, *res);
}

class NodeVisitorVisitVarTest : public ::testing::Test
{
protected:
	NodeVisitor visitor;
	Var *node;
	int expected;

	void SetUp() override
	{
		Token var_token(_ID, "foo");
		node = new Var(var_token);
		expected = 42;

		visitor.GLOBAL_SCOPE.set(node->value, expected);
	}

	void TearDown() override
	{
		node->delete_tree();
	}
};

TEST_F(NodeVisitorVisitVarTest, Valid)
{
	EXPECT_EQ(expected, visitor.visit(node));
}

TEST_F(NodeVisitorVisitVarTest, Invalid)
{
	try
	{
		Token var_token(_ID, "notExistVar");
		Var var_node(var_token);
		visitor.visit(&var_node);
		FAIL();
	}
	catch (const char *err)
	{
		EXPECT_STREQ("Invalid node type", err);
	}
}

class NodeVisitorVisitCompoundTest : public ::testing::Test
{
protected:
	NodeVisitor visitor;
	Compound *tree;

	void SetUp() override
	{
		std::string text = "\
			BEGIN\
				BEGIN\
					number := 2;\
					a := number;\
					b := 10 * a + 10 * number / 4;\
					c := a - - b\
				END;\
				x := 11;\
			END.";
		Lexer lexer(text);
		Parser parser(&lexer);
		tree = static_cast<Compound *>(parser.parse());
		visitor.visit(tree);
	}

	void TearDown() override
	{
		tree->delete_tree();
	}
};

TEST_F(NodeVisitorVisitCompoundTest, Valid)
{
	EXPECT_EQ(2, *visitor.GLOBAL_SCOPE.get("number"));
	EXPECT_EQ(*visitor.GLOBAL_SCOPE.get("number"), *visitor.GLOBAL_SCOPE.get("a"));
	EXPECT_EQ(
		10 * *visitor.GLOBAL_SCOPE.get("a") +
		10 * *visitor.GLOBAL_SCOPE.get("number") / 4,
		*visitor.GLOBAL_SCOPE.get("b")
	);
	EXPECT_EQ(
		*visitor.GLOBAL_SCOPE.get("a") - (-*visitor.GLOBAL_SCOPE.get("b")),
		*visitor.GLOBAL_SCOPE.get("c")
	);
	EXPECT_EQ(11, *visitor.GLOBAL_SCOPE.get("x"));
}

class InterpreterInterpretTest : public ::testing::Test
{
protected:
	Interpreter *interpreter;

	void SetUp() override
	{
		std::string text = "\
			BEGIN\
				BEGIN\
					number := 2;\
					a := number;\
					b := 10 * a + 10 * number / 4;\
					c := a - - b\
				END;\
				x := 11;\
			END.";
		Lexer lexer(text);
		Parser parser(&lexer);
		interpreter = new Interpreter(&parser);
		interpreter->interpret();
	}

	void TearDown() override
	{
		delete interpreter;
	}
};

TEST_F(InterpreterInterpretTest, Valid)
{
	EXPECT_EQ(2, *interpreter->GLOBAL_SCOPE.get("number"));
	EXPECT_EQ(*interpreter->GLOBAL_SCOPE.get("number"), *interpreter->GLOBAL_SCOPE.get("a"));
	EXPECT_EQ(
		10 * *interpreter->GLOBAL_SCOPE.get("a") +
		10 * *interpreter->GLOBAL_SCOPE.get("number") / 4,
		*interpreter->GLOBAL_SCOPE.get("b")
	);
	EXPECT_EQ(
		*interpreter->GLOBAL_SCOPE.get("a") - (-*interpreter->GLOBAL_SCOPE.get("b")),
		*interpreter->GLOBAL_SCOPE.get("c")
	);
	EXPECT_EQ(11, *interpreter->GLOBAL_SCOPE.get("x"));
}
