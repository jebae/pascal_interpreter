#include <iostream>
#include "setting.hpp"
#include "ast.hpp"
#include "token.hpp"
#include "lexer.hpp"
#include "parser.hpp"
#include "interpreter.hpp"

using namespace std;

int		main(void)
{
	string text = "BEGIN\
    	BEGIN\
			number := 2;\
			a := number;\
			b := 10 * a + 10 * number / 4;\
			c := a - - b\
		END;\
		x := 11;\
	END.";
	Lexer lexer(text);
	Parser parser(&lexer);
	Interpreter interpreter(&parser);

	interpreter.interpret();
	cout << *(interpreter.GLOBAL_SCOPE.get("number")) << endl;
	cout << *(interpreter.GLOBAL_SCOPE.get("a")) << endl;
	cout << *(interpreter.GLOBAL_SCOPE.get("b")) << endl;
	cout << *(interpreter.GLOBAL_SCOPE.get("c")) << endl;
	cout << *(interpreter.GLOBAL_SCOPE.get("x")) << endl;
	return (0);
}
