#ifndef TOKEN_HPP
# define TOKEN_HPP

# include <iostream>

class Token
{
public:
	int type;
	std::string value;

	Token(int type, std::string value);
	friend std::ostream & operator << (std::ostream &os, Token t);
};

#endif
