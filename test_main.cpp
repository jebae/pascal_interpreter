#include <gtest/gtest.h>
#include "ast.hpp"
#include "interpreter.hpp"
#include "lexer.hpp"
#include "parser.hpp"
#include "setting.hpp"
#include "token.hpp"

int		main(int argc, char **argv)
{
	::testing::InitGoogleTest(&argc, argv);
	//::testing::GTEST_FLAG(filter) = "ParserStatementListTest*";
	int res = RUN_ALL_TESTS();
	while (1);
	return (res);
}
