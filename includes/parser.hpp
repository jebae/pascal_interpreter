#ifndef PARSER_HPP
# define PARSER_HPP

# include "token.hpp"
# include <vector>

class Lexer;
class AST;

/*
** Parser make expression to AST tree
*/
class Parser
{
private:
	Lexer *lexer;
	Token current_token;
	void error(void);

public:
	Parser(Lexer *lexer);
	void eat(int token_type);
	AST *factor(void);
	AST *term(void);
	AST *expr(void);
	AST *program(void);
	AST *compound_statement(void);
	std::vector<AST *> statement_list(void);
	AST *statement(void);
	AST *assign_statement(void);
	AST *variable(void);
	AST *empty(void);
	AST *parse(void);
};

#endif
