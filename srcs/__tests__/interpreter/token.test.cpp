#include <gtest/gtest.h>
#include "token.hpp"
#include "setting.hpp"

using namespace std;

TEST(TokenTest, Osstream)
{
	vector<int> types = {
		_INTEGER, _PLUS, _SUB, _MUL,
		_DIV, _LPAREN, _RPAREN, _EOF,
		_ID, _BEGIN, _END, _ASSIGN,
		_SEMI, _DOT
	};
	vector<string> names = {
		"INTEGER", "PLUS", "SUB", "MUL",
		"DIV", "LPAREN", "RPAREN", "EOF",
		"ID", "BEGIN", "END", "ASSIGN",
		"SEMI", "DOT"
	};
	string dumb_value = "foo";
	vector<int>::iterator it_types = types.begin();
	vector<string>::iterator it_names = names.begin();

	while (it_types != types.end())
	{
		Token token(*it_types, dumb_value);
		testing::internal::CaptureStdout();
		cout << token;
		string output = testing::internal::GetCapturedStdout();
		string expected = "Token(" + *it_names + ", " + dumb_value + ")";
		EXPECT_STREQ(expected.c_str(), output.c_str());
		it_types++;
		it_names++;
	}
}
