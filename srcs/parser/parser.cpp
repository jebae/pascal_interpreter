#include "parser.hpp"
#include "setting.hpp"
#include "lexer.hpp"
#include "ast.hpp"
#include <stdexcept>

using namespace std;

Parser::Parser(Lexer *lexer)
: lexer(lexer), current_token(lexer->get_next_token())
{}

void Parser::error(void)
{
	throw "invalid syntax";
}

void Parser::eat(int token_type)
{
	if (current_token.type == token_type)
		current_token = lexer->get_next_token();
	else
		error();
}

AST* Parser::factor(void)
{
	Token token = current_token;

	if (current_token.type == _INTEGER)
	{
		eat(_INTEGER);
		Num *node = new Num(token);
		return (static_cast<AST *>(node));
	}
	else if (current_token.type == _LPAREN)
	{
		eat(_LPAREN);
		AST *node = expr();
		try
		{
			eat(_RPAREN);
		}
		catch (const char *err)
		{
			node->delete_tree();
			throw err;
		}
		return (node);
	}
	else if (current_token.type == _PLUS)
	{
		eat(_PLUS);
		UnaryOp *node = new UnaryOp(token, factor());
		return (static_cast<AST *>(node));
	}
	else if (current_token.type == _SUB)
	{
		eat(_SUB);
		UnaryOp *node = new UnaryOp(token, factor());
		return (static_cast<AST *>(node));
	}
	else
		return (variable());
	return (nullptr);
}

AST* Parser::term(void)
{
	AST *node = nullptr;
	AST *right = nullptr;

	node = factor();
	while (current_token.type == _MUL ||
		current_token.type == _DIV)
	{
		Token token = current_token;
		if (current_token.type == _MUL)
			eat(_MUL);
		else if (current_token.type == _DIV)
			eat(_DIV);
		try
		{
			right = factor();
		}
		catch (const char *err)
		{
			node->delete_tree();
			throw err;
		}
		node = static_cast<AST *>(new BinOp(token, node, right));
	}
	return (node);
}

AST* Parser::expr(void)
{
	AST *node = nullptr;
	AST *right = nullptr;

	node = term();
	while (current_token.type == _PLUS ||
		current_token.type == _SUB)
	{
		Token token = current_token;
		if (current_token.type == _PLUS)
			eat(_PLUS);
		else if (current_token.type == _SUB)
			eat(_SUB);
		try
		{
			right = term();
		}
		catch (const char *err)
		{
			node->delete_tree();
			throw err;
		}
		node = static_cast<AST *>(new BinOp(token, node, right));
	}
	return (node);
}

AST *Parser::parse(void)
{
	AST *node = program();

	if (current_token.type != _EOF)
	{
		node->delete_tree();
		error();
	}
	return (node);
}

AST* Parser::program(void)
{
	AST *node = compound_statement();

	try
	{
		eat(_DOT);
	}
	catch (const char *err)
	{
		node->delete_tree();
		throw err;
	}
	return (node);
}

AST* Parser::compound_statement(void)
{
	eat(_BEGIN);
	vector<AST *> children = statement_list();
	try
	{
		eat(_END);
	}
	catch (const char *err)
	{
		for_each(
			children.begin(),
			children.end(),
			[] (AST *node) {
				node->delete_tree();
			}
		);
		throw err;
	}
	Compound *node = new Compound(children);
	return (static_cast<AST *>(node));
}

vector<AST *> Parser::statement_list(void)
{
	AST *node = statement();
	vector<AST *>list = {node};
	auto delete_list = [&list] ()
	{
		for_each(
			list.begin(),
			list.end(),
			[] (AST *node) {
				node->delete_tree();
			}
		);
	};

	while (current_token.type == _SEMI)
	{
		eat(_SEMI);
		try
		{
			list.push_back(statement());
		}
		catch (const char *err)
		{
			delete_list();
			throw err;
		}
	}
	if (current_token.type != _END && current_token.type != _EOF)
	{
		delete_list();
		error();
	}
	return (list);
}

AST* Parser::statement(void)
{
	if (current_token.type == _BEGIN)
		return (compound_statement());
	else if (current_token.type == _ID)
		return (assign_statement());
	else
		return (empty());
}

AST* Parser::assign_statement(void)
{
	AST *left = variable();
	AST *right = nullptr;
	Token token = current_token;
	try
	{
		eat(_ASSIGN);
		right = expr();
	}
	catch (const char *err)
	{
		left->delete_tree();
		throw err;
	}
	Assign *node = new Assign(token, left, right);
	return (static_cast<AST *>(node));
}

AST* Parser::variable(void)
{
	Var *node;
	Token token = current_token;

	eat(_ID);
	node = new Var(token);
	return (static_cast<AST *>(node));
}

AST* Parser::empty(void)
{
	NoOp *node = new NoOp();

	return (static_cast<AST *>(node));
}
