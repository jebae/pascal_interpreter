#include "setting.hpp"
#include "lexer.hpp"

using namespace std;

Lexer::Lexer(string &text)
: text(text),
reserved_keywords(HashMap<Token>(Token(_EOF, "")))
{
	pos = 0;
	len_text = text.length();
	current_char = text[0];
	reserved_keywords.set("BEGIN", Token(_BEGIN, "BEGIN"));
	reserved_keywords.set("END", Token(_END, "END"));
}

void Lexer::error(void)
{
	throw "invalid character";
}

void Lexer::advance(void)
{
	pos++;
	if (pos >= len_text)
		current_char = '\0';
	else
		current_char = text[pos];
}

void Lexer::skip_whitespace(void)
{
	while (current_char != '\0' &&
		(current_char == ' ' || current_char == '\n' || current_char == '\t'))
		advance();
}

char Lexer::peek(void)
{
	size_t peek_pos = pos + 1;

	if (peek_pos >= len_text)
		return ('\0');
	return (text[peek_pos]);
}

string Lexer::integer(void)
{
	int offset_pos;

	offset_pos = pos;
	while (isdigit(current_char))
		advance();
	return (string(text.begin() + offset_pos, text.begin() + pos));
}

Token Lexer::_id(void)
{
	string name = "";

	while (current_char != '\0' && isalnum(current_char))
	{
		name += current_char;
		advance();
	}
	Token *res = reserved_keywords.get(name);
	if (res == nullptr)
		return (Token(_ID, name));
	return (*res);
}

Token Lexer::get_next_token(void)
{
	Token token(_EOF, "");

	skip_whitespace();
	if (current_char == '\0')
		return (token);
	else if (isdigit(current_char))
		return (Token(_INTEGER, integer()));
	else if (isalpha(current_char))
		return (_id());
	else if (current_char == ':' && peek() == '=')
	{
		token = Token(_ASSIGN, ":=");
		advance();
	}
	else if (current_char == ';')
		token = Token(_SEMI, ";");
	else if (current_char == '.')
		token = Token(_DOT, ".");
	else if (current_char == '+')
		token = Token(_PLUS, "+");
	else if (current_char == '-')
		token = Token(_SUB, "-");
	else if (current_char == '*')
		token = Token(_MUL, "*");
	else if (current_char == '/')
		token = Token(_DIV, "/");
	else if (current_char == '(')
		token = Token(_LPAREN, "(");
	else if (current_char == ')')
		token = Token(_RPAREN, ")");
	else
		error();
	advance();
	return (token);
}
