#ifndef SETTING_HPP
# define SETTING_HPP

enum E_TYPE
{
	_INTEGER = 0,
	_PLUS,
	_SUB,
	_MUL,
	_DIV,
	_LPAREN,
	_RPAREN,
	_ID,
	_BEGIN,
	_END,
	_ASSIGN,
	_SEMI,
	_DOT,
	_EOF
};

#endif
