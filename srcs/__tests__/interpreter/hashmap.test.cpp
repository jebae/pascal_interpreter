#include <gtest/gtest.h>
#include "hashmap.hpp"

using namespace std;

class HashMapTest : public ::testing::Test
{
protected:
	HashMap<string> hashmap;
	string name;
	string company;

	HashMapTest()
	: hashmap("")
	{
		name = "jebae";
		company = "line";

		hashmap.set("name", name);
		hashmap.set("company", company);
	}
};

TEST_F(HashMapTest, Get)
{
	EXPECT_STREQ(hashmap.get("name")->c_str(), name.c_str());
	EXPECT_STREQ(hashmap.get("company")->c_str(), company.c_str());
}

TEST_F(HashMapTest, SetNewValue)
{
	string new_name = "hybae";

	EXPECT_EQ(1, hashmap.set("name", new_name));
	EXPECT_STREQ(hashmap.get("name")->c_str(), new_name.c_str());
}

TEST_F(HashMapTest, Remove)
{
	hashmap.remove("name");

	EXPECT_EQ(hashmap.get("name"), nullptr);
	EXPECT_STREQ(hashmap.get("company")->c_str(), company.c_str());
}

TEST_F(HashMapTest, SetMoreThanSize)
{
	for (int i=0; i < HASHMAP_TABLE_SIZE - 2; i++)
		EXPECT_EQ(1, hashmap.set("foo" + to_string(i), "bar"));

	EXPECT_STREQ(hashmap.get("name")->c_str(), name.c_str());
	EXPECT_STREQ(hashmap.get("company")->c_str(), company.c_str());
	EXPECT_EQ(0, hashmap.set("too", "bar"));
}
