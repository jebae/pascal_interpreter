#ifndef INTERPRETER_HPP
# define INTERPRETER_HPP

# include "hashmap.hpp"

class AST;
class BinOp;
class Num;
class UnaryOp;
class Compound;
class Assign;
class Var;
class NoOp;
class Parser;

class NodeVisitor
{
private:
	void error(void);

public:
	HashMap<int> GLOBAL_SCOPE;

	NodeVisitor(void);
	int visit(BinOp *node);
	int visit(Num *node);
	int visit(UnaryOp *node);
	int visit(Compound *node);
	int visit(Assign *node);
	int visit(Var *node);
	int visit(NoOp *node);
};

class Interpreter : public NodeVisitor
{
private:
	Parser *parser;

public:
	Interpreter(Parser *parser);
	int interpret(void);
};

#endif
