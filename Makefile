# utils
KRED = \033[0;31m
KGRN = \033[0;32m
KYEL = \033[0;33m
KNRM = \033[0m
COUNTER = 0

define compile_obj
	@printf "$(KGRN)[pascal_interpreter]$(KNRM) compile $(1)\n"
	$(CC) $(CFLAGS) $(INCLUDES) -c $(1) -o $(2)
	$(eval COUNTER=$(shell echo $$(($(COUNTER) + 1))))
endef

# program name
NAME = pascal_interpreter

# compiler
CC = g++

# path
SRCDIR = srcs

INCDIR = includes

OBJDIR = objs

# compiler options
CFLAGS = -Wall -Wextra -Werror -std=c++11

INCLUDES = -I ./$(INCDIR)

# srcs
SRC_INTERPRETER = interpreter.cpp\
	token.cpp\

SRC_PARSER = ast.cpp\
	parser.cpp\

SRC_LEXER = lexer.cpp\

# objs
OBJS = $(addprefix $(OBJDIR)/,$(SRC_INTERPRETER:.cpp=.o))
OBJS += $(addprefix $(OBJDIR)/,$(SRC_PARSER:.cpp=.o))
OBJS += $(addprefix $(OBJDIR)/,$(SRC_LEXER:.cpp=.o))

# compile objs
HEADERS = $(INCDIR)/setting.hpp\
	$(INCDIR)/interpreter.hpp\
	$(INCDIR)/token.hpp\
	$(INCDIR)/hashmap.hpp\
	$(INCDIR)/ast.hpp\
	$(INCDIR)/lexer.hpp\

# this is main
$(OBJDIR)/%.o : $(SRCDIR)/%.cpp $(HEADERS)
	@$(call compile_obj,$<,$@)

$(OBJDIR)/%.o : $(SRCDIR)/interpreter/%.cpp $(HEADERS)
	@$(call compile_obj,$<,$@)

$(OBJDIR)/%.o : $(SRCDIR)/parser/%.cpp $(HEADERS)
	@$(call compile_obj,$<,$@)

$(OBJDIR)/%.o : $(SRCDIR)/lexer/%.cpp $(HEADERS)
	@$(call compile_obj,$<,$@)

# build
all : $(NAME)

$(NAME) : pre_build $(OBJDIR) $(OBJS) post_build

$(OBJDIR) :
	@mkdir -p $(OBJDIR)

pre_build :
	@printf "$(KGRN)[pascal_interpreter] $(KYEL)build $(NAME)\n$(KNRM)"

post_build :
	@printf "$(KGRN)[pascal_interpreter] $(KYEL)$(COUNTER) files compiled\n$(KNRM)"

# command
test : all
	@$(CC) $(CFLAGS) $(INCLUDES) -I $(GTEST_DIR)/include -L $(GTEST_DIR)/lib -lgtest -lpthread $(OBJS) test_main.cpp $(SRCDIR)/__tests__/*/*.test.cpp -o test
	
clean :
	@rm -rf $(OBJS)

fclean : clean
	@rm -rf $(NAME)

re : fclean all

.PHONY : all pre_build post_build clean fclean re
