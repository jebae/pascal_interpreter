#include "interpreter.hpp"
#include "ast.hpp"
#include "parser.hpp"
#include "setting.hpp"
#include <stdexcept>

using namespace std;

/*
** NodeVisitor
*/
NodeVisitor::NodeVisitor(void)
: GLOBAL_SCOPE(HashMap<int>(0))
{}

void NodeVisitor::error(void)
{
	throw "Invalid node type";
}

int NodeVisitor::visit(BinOp *node)
{
	if (node->token.type == _PLUS)
		return (
			node->left->accept_visitor(this) +
			node->right->accept_visitor(this)
		);
	else if (node->token.type == _SUB)
		return (
			node->left->accept_visitor(this) -
			node->right->accept_visitor(this)
		);
	else if (node->token.type == _MUL)
		return (
			node->left->accept_visitor(this) *
			node->right->accept_visitor(this)
		);
	else if (node->token.type == _DIV)
		return (
			node->left->accept_visitor(this) /
			node->right->accept_visitor(this)
		);
	error();
	return (0);
}

int NodeVisitor::visit(Num *node)
{
	return (node->value);
}

int NodeVisitor::visit(UnaryOp *node)
{
	if (node->token.type == _PLUS)
		return (node->expr->accept_visitor(this));
	if (node->token.type == _SUB)
		return (-1 * node->expr->accept_visitor(this));
	error();
	return (0);
}

int NodeVisitor::visit(Compound *node)
{
	NodeVisitor *visitor = this;

	for_each(
		node->children.begin(),
		node->children.end(),
		[visitor] (AST *child) {
			child->accept_visitor(visitor);
		}
	);
	return (1);
}

int NodeVisitor::visit(Assign *node)
{
	GLOBAL_SCOPE.set(
		(static_cast<Var *>(node->left))->value,
		node->right->accept_visitor(this)
	);
	return (1);
}

int NodeVisitor::visit(Var *node)
{
	int *value = GLOBAL_SCOPE.get(node->value);

	if (value == nullptr)
		error();
	return (*value);
}

int NodeVisitor::visit(NoOp *node)
{
	(void)(node);
	return (1);
}

/*
** Interpreter
*/
Interpreter::Interpreter(Parser *parser)
: parser(parser)
{}

int Interpreter::interpret(void)
{
	AST *tree = parser->parse();

	tree->accept_visitor(static_cast<NodeVisitor *>(this));
	tree->delete_tree();
	return (1);
}
