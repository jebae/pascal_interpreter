#ifndef AST_HPP
# define AST_HPP

# include "token.hpp"
# include <vector>

class NodeVisitor;

class AST
{
private:

public:
	Token token;

	AST(Token token);
	virtual ~AST() {};
	virtual int accept_visitor(NodeVisitor *visitor) = 0;
	virtual void delete_tree(void) = 0;
};

class BinOp : public AST
{
public:
	AST *left;
	AST *right;

	BinOp(Token &token, AST *left, AST *right);
	int accept_visitor(NodeVisitor *visitor);
	void delete_tree(void);
};

class Num : public AST
{
public:
	int value;

	Num(Token &token);
	int accept_visitor(NodeVisitor *visitor);
	void delete_tree(void);
};

class UnaryOp : public AST
{
public:
	AST *expr;

	UnaryOp(Token &token, AST *expr);
	int accept_visitor(NodeVisitor *visitor);
	void delete_tree(void);
};

class Compound : public AST
{
public:
	std::vector<AST *> children;

	Compound(std::vector<AST *> children);
	int accept_visitor(NodeVisitor *visitor);
	void delete_tree(void);
};

class Assign : public AST
{
public:
	AST *left;
	AST *right;

	Assign(Token &token, AST *left, AST *right);
	int accept_visitor(NodeVisitor *visitor);
	void delete_tree(void);
};

class Var : public AST
{
public:
	std::string value;

	Var(Token &token);
	int accept_visitor(NodeVisitor *visitor);
	void delete_tree(void);
};

class NoOp : public AST
{
public:
	NoOp(void);
	int accept_visitor(NodeVisitor *visitor);
	void delete_tree(void);
};

#endif
