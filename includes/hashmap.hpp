#ifndef HASHMAP_HPP
# define HASHMAP_HPP
# include <string>

# define HASHMAP_TABLE_SIZE 100

using namespace std;

template <typename T>
class HashNode
{
public:
	string key;
	T value;

	HashNode(string &key, T &value);
};

template <typename T>
HashNode<T>::HashNode(string &key, T &value)
: key(key), value(value)
{}

template <typename T>
class HashMap
{
private:
	HashNode<T> *table[HASHMAP_TABLE_SIZE];
	HashNode<T> *dumb_ptr;
	size_t occupied;

	unsigned int hash_fn(string key);
	HashNode<T> **search(string key);

public:
	HashMap(T dumb_value);
	~HashMap(void);
	int set(string key, T value);
	T *get(string key);
	void remove(string key);
};

template <typename T>
HashMap<T>::HashMap(T dumb_value)
{
	for (int i=0; i < HASHMAP_TABLE_SIZE; i++)
		table[i] = nullptr;

	// make dumb_ptr
	string key = "";
	dumb_ptr = new HashNode<T>(key, dumb_value);
	occupied = 0;
}

template <typename T>
HashMap<T>::~HashMap(void)
{
	for (int i=0; i < HASHMAP_TABLE_SIZE; i++)
	{
		if (table[i] != nullptr && table[i] != dumb_ptr)
			delete table[i];
	}
	delete dumb_ptr;
}

template <typename T>
unsigned int HashMap<T>::hash_fn(string key)
{
	unsigned int *ptr = reinterpret_cast<unsigned int *>(&key);

	return (*ptr % HASHMAP_TABLE_SIZE);
}

template <typename T>
HashNode<T>** HashMap<T>::search(string key)
{
	unsigned int hash = hash_fn(key);
	int i;

	i = 0;
	while (table[hash] != nullptr && i < HASHMAP_TABLE_SIZE)
	{
		if (table[hash] != dumb_ptr && table[hash]->key == key)
			return (&(table[hash]));
		hash++;
		hash %= HASHMAP_TABLE_SIZE;
		i++;
	}
	return (nullptr);
}

template <typename T>
int HashMap<T>::set(string key, T value)
{
	HashNode<T> **node = search(key);

	// update node
	if (node != nullptr)
	{
		(*node)->value = value;
		return (1);
	}
	// insert new node
	unsigned int hash = hash_fn(key);

	if (occupied == HASHMAP_TABLE_SIZE)
		return (0);
	while (table[hash] != nullptr && table[hash] != dumb_ptr)
	{
		hash++;
		hash %= HASHMAP_TABLE_SIZE;
	}
	table[hash] = new HashNode<T>(key, value);
	occupied++;
	return (1);
}

template <typename T>
T* HashMap<T>::get(string key)
{
	HashNode<T> **node = search(key);

	if (node == nullptr)
		return (nullptr);
	return (&((*node)->value));
}
 
template <typename T>
void HashMap<T>::remove(string key)
{
	HashNode<T> **node = search(key);

	if (node == nullptr)
		return ;
	delete *node;
	*node = dumb_ptr;
}

#endif
