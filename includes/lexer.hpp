#ifndef LEXER_HPP
# define LEXER_HPP

# include "token.hpp"
# include "hashmap.hpp"

class Lexer
{
private:
	char current_char;
	size_t len_text;
	unsigned int pos;
	std::string text;
	HashMap<Token> reserved_keywords;

	void error(void);

public:
	Lexer(std::string &text);
	void advance(void);
	void skip_whitespace(void);
	char peek(void);
	std::string integer(void);
	Token _id(void);
	Token get_next_token(void);
};

#endif
