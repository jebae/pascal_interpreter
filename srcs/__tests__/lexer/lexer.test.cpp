#include <gtest/gtest.h>
#include "lexer.hpp"
#include "setting.hpp"

TEST(LexerTest, AdvanceAndPeek)
{
	string text = "BEGIN";
	Lexer lexer(text);
	char ch;

	ch = lexer.peek();
	EXPECT_EQ('E', ch);
	lexer.advance();
	ch = lexer.peek();
	EXPECT_EQ('G', ch);
}

TEST(LexerTest, Integer)
{
	string text = "123 45";
	Lexer lexer(text);
	string num;

	num = lexer.integer();
	EXPECT_STREQ("123", num.c_str());
}

TEST(LexerTest, IdNotReserved)
{
	string text = "var = 2";
	Lexer lexer(text);
	Token token = lexer._id();

	EXPECT_EQ(_ID, token.type);
	EXPECT_STREQ("var", token.value.c_str());
}

TEST(LexerTest, IdReserved)
{
	// "BEGIN" and "END" are reserved when lexer is initialized
	string text = "BEGIN"; 
	Lexer lexer(text);
	Token token = lexer._id();

	EXPECT_EQ(_BEGIN, token.type);
	EXPECT_STREQ("BEGIN", token.value.c_str());
}

TEST(LexerTest, GetNextTokenArithmetic)
{
	string text = "(123 + 456) * 78 / 3 - 0"; 
	Lexer lexer(text);

	Token token = lexer.get_next_token();
	EXPECT_EQ(_LPAREN, token.type);
	EXPECT_STREQ("(", token.value.c_str());

	token = lexer.get_next_token();
	EXPECT_EQ(_INTEGER, token.type);
	EXPECT_STREQ("123", token.value.c_str());

	token = lexer.get_next_token();
	EXPECT_EQ(_PLUS, token.type);
	EXPECT_STREQ("+", token.value.c_str());

	token = lexer.get_next_token();
	EXPECT_EQ(_INTEGER, token.type);
	EXPECT_STREQ("456", token.value.c_str());

	token = lexer.get_next_token();
	EXPECT_EQ(_RPAREN, token.type);
	EXPECT_STREQ(")", token.value.c_str());

	token = lexer.get_next_token();
	EXPECT_EQ(_MUL, token.type);
	EXPECT_STREQ("*", token.value.c_str());

	token = lexer.get_next_token();
	EXPECT_EQ(_INTEGER, token.type);
	EXPECT_STREQ("78", token.value.c_str());

	token = lexer.get_next_token();
	EXPECT_EQ(_DIV, token.type);
	EXPECT_STREQ("/", token.value.c_str());

	token = lexer.get_next_token();
	EXPECT_EQ(_INTEGER, token.type);
	EXPECT_STREQ("3", token.value.c_str());

	token = lexer.get_next_token();
	EXPECT_EQ(_SUB, token.type);
	EXPECT_STREQ("-", token.value.c_str());

	token = lexer.get_next_token();
	EXPECT_EQ(_INTEGER, token.type);
	EXPECT_STREQ("0", token.value.c_str());
}

TEST(LexerTest, GetNextToken)
{
	string text = "\
		BEGIN\
			BEGIN\
				number := 2;\
				a := number;\
				b := 10 * a + 10 * number / 4;\
				c := a - - b\
			END;\
			x := 11;\
		END."; 
	Lexer lexer(text);

	Token token = lexer.get_next_token();
	EXPECT_EQ(_BEGIN, token.type);
	EXPECT_STREQ("BEGIN", token.value.c_str());

	token = lexer.get_next_token();
	EXPECT_EQ(_BEGIN, token.type);
	EXPECT_STREQ("BEGIN", token.value.c_str());

	token = lexer.get_next_token();
	EXPECT_EQ(_ID, token.type);
	EXPECT_STREQ("number", token.value.c_str());

	token = lexer.get_next_token();
	EXPECT_EQ(_ASSIGN, token.type);
	EXPECT_STREQ(":=", token.value.c_str());

	token = lexer.get_next_token();
	EXPECT_EQ(_INTEGER, token.type);
	EXPECT_STREQ("2", token.value.c_str());

	token = lexer.get_next_token();
	EXPECT_EQ(_SEMI, token.type);
	EXPECT_STREQ(";", token.value.c_str());

	token = lexer.get_next_token();
	EXPECT_EQ(_ID, token.type);
	EXPECT_STREQ("a", token.value.c_str());

	token = lexer.get_next_token();
	EXPECT_EQ(_ASSIGN, token.type);
	EXPECT_STREQ(":=", token.value.c_str());

	token = lexer.get_next_token();
	EXPECT_EQ(_ID, token.type);
	EXPECT_STREQ("number", token.value.c_str());
}

TEST(LexerTest, GetNextTokenCaseInvalidChar)
{
	string text = "foo @ bar";
	Lexer lexer(text);

	lexer.get_next_token();

	try
	{
		lexer.get_next_token();
		FAIL();
	}
	catch (const char *err)
	{
		EXPECT_STREQ("invalid character", err);
	}
}
