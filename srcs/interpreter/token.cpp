#include "setting.hpp"
#include "token.hpp"

using namespace std;

Token::Token(int type, string value)
: type(type), value(value)
{}

ostream & operator << (ostream &os, Token t)
{
	string s;

	switch (t.type)
	{
		case _INTEGER:
			s = "INTEGER";
			break ;
		case _PLUS:
			s = "PLUS";
			break ;
		case _SUB:
			s = "SUB";
			break ;
		case _MUL:
			s = "MUL";
			break ;
		case _DIV:
			s = "DIV";
			break ;
		case _LPAREN:
			s = "LPAREN";
			break ;
		case _RPAREN:
			s = "RPAREN";
			break ;
		case _EOF:
			s = "EOF";
			break ;
		case _ID:
			s = "ID";
			break ;
		case _BEGIN:
			s = "BEGIN";
			break ;
		case _END:
			s = "END";
			break ;
		case _ASSIGN:
			s = "ASSIGN";
			break ;
		case _SEMI:
			s = "SEMI";
			break ;
		case _DOT:
			s = "DOT";
			break ;
		default:
			break ;
	}
	os << "Token(" << s << ", " << t.value << ")";
	return (os);
}
