#include "ast.hpp"
#include "interpreter.hpp"
#include "setting.hpp"

using namespace std;

/*
** AST
*/
AST::AST(Token token)
:token(token)
{}

/*
** BinOp
*/
BinOp::BinOp(Token &token, AST *left, AST *right)
: AST(token), left(left), right(right)
{}

int BinOp::accept_visitor(NodeVisitor *visitor)
{
	return (visitor->visit(this));
}

void BinOp::delete_tree(void)
{
	left->delete_tree();
	right->delete_tree();
	delete this;
}

/*
** Num
*/
Num::Num(Token &token)
: AST(token)
{
	value = stoi(token.value);
}

int Num::accept_visitor(NodeVisitor *visitor)
{
	return (visitor->visit(this));
}

void Num::delete_tree(void)
{
	delete this;
}

/*
** UnaryOp
*/
UnaryOp::UnaryOp(Token &token, AST *expr)
: AST(token), expr(expr)
{}

int UnaryOp::accept_visitor(NodeVisitor *visitor)
{
	return (visitor->visit(this));
}

void UnaryOp::delete_tree(void)
{
	expr->delete_tree();
	delete this;
}

/*
** Compound
** for Compound node, token has no meaning
*/
Compound::Compound(vector<AST *> children)
: AST(Token(_EOF, "")), children(children)
{}

int Compound::accept_visitor(NodeVisitor *visitor)
{
	return (visitor->visit(this));
}

void Compound::delete_tree(void)
{
	for_each(
		this->children.begin(),
		this->children.end(),
		[] (AST *node) {
			node->delete_tree();
		}
	);
	delete this;
}

/*
** Assign
*/
Assign::Assign(Token &token, AST *left, AST *right)
: AST(token), left(left), right(right)
{}

int Assign::accept_visitor(NodeVisitor *visitor)
{
	return (visitor->visit(this));
}

void Assign::delete_tree(void)
{
	left->delete_tree();
	right->delete_tree();
	delete this;
}

/*
** Var
*/
Var::Var(Token &token)
: AST(token), value(token.value)
{}

int Var::accept_visitor(NodeVisitor *visitor)
{
	return (visitor->visit(this));
}

void Var::delete_tree(void)
{
	delete this;
}

/*
** NoOp
*/
NoOp::NoOp(void)
: AST(Token(_EOF, ""))
{}

int NoOp::accept_visitor(NodeVisitor *visitor)
{
	return (visitor->visit(this));
}

void NoOp::delete_tree(void)
{
	delete this;
}
