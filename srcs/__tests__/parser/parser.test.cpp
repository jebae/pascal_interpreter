#include <gtest/gtest.h>
#include "parser.hpp"
#include "lexer.hpp"
#include "ast.hpp"
#include "setting.hpp"

class ParserEatTest : public ::testing::Test
{
protected:
	Lexer *lexer;
	Parser *parser;

	void SetUp()
	{
		std::string text = "123 + 90";
		lexer = new Lexer(text);
		parser = new Parser(lexer);
	}

	void TearDown()
	{
		delete lexer;
		delete parser;
	}
};

TEST_F(ParserEatTest, Valide)
{
	parser->eat(_INTEGER);
}

TEST_F(ParserEatTest, Invalide)
{
	try
	{
		parser->eat(_PLUS);
	}
	catch (const char *err)
	{
		EXPECT_STREQ("invalid syntax", err);
	}
}

class ParserTest : public ::testing::Test
{
protected:
	AST *node;

	void TearDown() override
	{
		if (node != nullptr)
			node->delete_tree();
	}
};

TEST_F(ParserTest, FactorInteger)
{
	std::string text = "123 + 90";
	Lexer lexer(text);
	Parser parser(&lexer);
	Num *num_node;

	node = parser.factor();
	num_node = static_cast<Num *>(node);
	EXPECT_EQ(_INTEGER, num_node->token.type);
	EXPECT_STREQ("123", num_node->token.value.c_str());
}

TEST_F(ParserTest, FactorUnaryPlus)
{
	std::string text = "+123";
	Lexer lexer(text);
	Parser parser(&lexer);
	UnaryOp *unary_node;
	Num *num_node;

	node = parser.factor();
	unary_node = static_cast<UnaryOp *>(node);
	EXPECT_EQ(_PLUS, unary_node->token.type);

	num_node = static_cast<Num *>(unary_node->expr);
	EXPECT_EQ(_INTEGER, num_node->token.type);
	EXPECT_STREQ("123", num_node->token.value.c_str());
}

TEST_F(ParserTest, FactorUnaryPlusPlus)
{
	std::string text = "+ +123";
	Lexer lexer(text);
	Parser parser(&lexer);
	UnaryOp *unary_node;
	Num *num_node;

	node = parser.factor();
	unary_node = static_cast<UnaryOp *>(node);
	EXPECT_EQ(_PLUS, unary_node->token.type);

	unary_node = static_cast<UnaryOp *>(unary_node->expr);
	EXPECT_EQ(_PLUS, unary_node->token.type);

	num_node = static_cast<Num *>(unary_node->expr);
	EXPECT_EQ(_INTEGER, num_node->token.type);
	EXPECT_STREQ("123", num_node->token.value.c_str());
}

TEST_F(ParserTest, FactorUnarySub)
{
	std::string text = "-123";
	Lexer lexer(text);
	Parser parser(&lexer);
	UnaryOp *unary_node;
	Num *num_node;

	node = parser.factor();
	unary_node = static_cast<UnaryOp *>(node);
	EXPECT_EQ(_SUB, unary_node->token.type);

	num_node = static_cast<Num *>(unary_node->expr);
	EXPECT_EQ(_INTEGER, num_node->token.type);
	EXPECT_STREQ("123", num_node->token.value.c_str());
}

TEST_F(ParserTest, FactorUnarySubSub)
{
	std::string text = "- -123";
	Lexer lexer(text);
	Parser parser(&lexer);
	UnaryOp *unary_node;
	Num *num_node;

	node = parser.factor();
	unary_node = static_cast<UnaryOp *>(node);
	EXPECT_EQ(_SUB, unary_node->token.type);

	unary_node = static_cast<UnaryOp *>(unary_node->expr);
	EXPECT_EQ(_SUB, unary_node->token.type);

	num_node = static_cast<Num *>(unary_node->expr);
	EXPECT_EQ(_INTEGER, num_node->token.type);
	EXPECT_STREQ("123", num_node->token.value.c_str());
}

TEST_F(ParserTest, FactorUnarySubSubVar)
{
	std::string text = "- -var";
	Lexer lexer(text);
	Parser parser(&lexer);
	UnaryOp *unary_node;
	Var *var_node;

	node = parser.factor();
	unary_node = static_cast<UnaryOp *>(node);
	EXPECT_EQ(_SUB, unary_node->token.type);

	unary_node = static_cast<UnaryOp *>(unary_node->expr);
	EXPECT_EQ(_SUB, unary_node->token.type);

	var_node = static_cast<Var *>(unary_node->expr);
	EXPECT_EQ(_ID, var_node->token.type);
	EXPECT_STREQ("var", var_node->token.value.c_str());
}

TEST_F(ParserTest, FactorParenthesisCaseTerm)
{
	std::string text = "(123 / 7)";
	Lexer lexer(text);
	Parser parser(&lexer);
	BinOp *bin_node;
	Num *num_node;

	node = parser.factor();
	bin_node = static_cast<BinOp *>(node);
	EXPECT_EQ(_DIV, bin_node->token.type);

	num_node = static_cast<Num *>(bin_node->left);
	EXPECT_EQ(_INTEGER, num_node->token.type);
	EXPECT_STREQ("123", num_node->token.value.c_str());

	num_node = static_cast<Num *>(bin_node->right);
	EXPECT_EQ(_INTEGER, num_node->token.type);
	EXPECT_STREQ("7", num_node->token.value.c_str());
}

TEST_F(ParserTest, FactorParenthesisCaseExpr)
{
	std::string text = "(123 - 7)";
	Lexer lexer(text);
	Parser parser(&lexer);
	BinOp *bin_node;
	Num *num_node;

	node = parser.factor();
	bin_node = static_cast<BinOp *>(node);
	EXPECT_EQ(_SUB, bin_node->token.type);

	num_node = static_cast<Num *>(bin_node->left);
	EXPECT_EQ(_INTEGER, num_node->token.type);
	EXPECT_STREQ("123", num_node->token.value.c_str());

	num_node = static_cast<Num *>(bin_node->right);
	EXPECT_EQ(_INTEGER, num_node->token.type);
	EXPECT_STREQ("7", num_node->token.value.c_str());
}

TEST_F(ParserTest, TermMul)
{
	std::string text = "123 * -7";
	Lexer lexer(text);
	Parser parser(&lexer);
	BinOp *bin_node;
	Num *num_node;
	UnaryOp *unary_node;

	node = parser.term();
	bin_node = static_cast<BinOp *>(node);
	EXPECT_EQ(_MUL, bin_node->token.type);

	num_node = static_cast<Num *>(bin_node->left);
	EXPECT_EQ(_INTEGER, num_node->token.type);
	EXPECT_STREQ("123", num_node->token.value.c_str());

	unary_node = static_cast<UnaryOp *>(bin_node->right);
	EXPECT_EQ(_SUB, unary_node->token.type);

	num_node = static_cast<Num *>(unary_node->expr);
	EXPECT_EQ(_INTEGER, num_node->token.type);
	EXPECT_STREQ("7", num_node->token.value.c_str());
}

TEST_F(ParserTest, ExprPlus)
{
	std::string text = "123 + -7";
	Lexer lexer(text);
	Parser parser(&lexer);
	BinOp *bin_node;
	Num *num_node;
	UnaryOp *unary_node;

	node = parser.expr();
	bin_node = static_cast<BinOp *>(node);
	EXPECT_EQ(_PLUS, bin_node->token.type);

	num_node = static_cast<Num *>(bin_node->left);
	EXPECT_EQ(_INTEGER, num_node->token.type);
	EXPECT_STREQ("123", num_node->token.value.c_str());

	unary_node = static_cast<UnaryOp *>(bin_node->right);
	EXPECT_EQ(_SUB, unary_node->token.type);

	num_node = static_cast<Num *>(unary_node->expr);
	EXPECT_EQ(_INTEGER, num_node->token.type);
	EXPECT_STREQ("7", num_node->token.value.c_str());
}

TEST_F(ParserTest, AssignValid)
{
	std::string text = "age := 28 + 3";
	Lexer lexer(text);
	Parser parser(&lexer);
	Assign *assign_node;
	Var *var_node;
	BinOp *bin_node;

	node = parser.assign_statement();
	assign_node = static_cast<Assign *>(node);
	EXPECT_EQ(_ASSIGN, assign_node->token.type);

	var_node = static_cast<Var *>(assign_node->left);
	EXPECT_EQ(_ID, var_node->token.type);
	EXPECT_STREQ("age", var_node->token.value.c_str());

	bin_node = static_cast<BinOp *>(assign_node->right);
	EXPECT_EQ(_PLUS, bin_node->token.type);
}

TEST_F(ParserTest, AssignInvalidCharacter)
{
	std::string text = "age = 28 + 3";
	Lexer lexer(text);
	Parser parser(&lexer);

	try
	{
		node = parser.assign_statement();
		FAIL();
	}
	catch (const char *err)
	{
		EXPECT_STREQ("invalid character", err);
		node = nullptr; // not to seg fault in TearDown
	}
}

TEST_F(ParserTest, Compound)
{
	std::string text = "BEGIN\
			a := 1;\
			b := a + 2;\
		END";
	Lexer lexer(text);
	Parser parser(&lexer);
	Compound *compound_node;

	node = parser.compound_statement();
	compound_node = static_cast<Compound *>(node);
	EXPECT_EQ((unsigned long)3, compound_node->children.size());
}

TEST_F(ParserTest, StatementBegin)
{
	std::string text = "BEGIN\
			a := 1;\
			b := a + 2;\
		END";
	Lexer lexer(text);
	Parser parser(&lexer);
	Compound *compound_node;

	node = parser.statement();
	compound_node = static_cast<Compound *>(node);
	EXPECT_EQ((unsigned long)3, compound_node->children.size());
}

TEST_F(ParserTest, StatementAssign)
{
	std::string text = "a := 42";
	Lexer lexer(text);
	Parser parser(&lexer);
	Assign *assign_node;
	Var *left;
	Num *right;

	node = parser.statement();
	assign_node = static_cast<Assign *>(node);

	left = static_cast<Var *>(assign_node->left);
	EXPECT_EQ(_ID, left->token.type);
	EXPECT_STREQ("a", left->token.value.c_str());

	right = static_cast<Num *>(assign_node->right);
	EXPECT_EQ(_INTEGER, right->token.type);
	EXPECT_STREQ("42", right->token.value.c_str());
}

class ParserStatementListTest : public ::testing::Test
{
protected:
	vector<AST *> list;

	void TearDown()
	{
		for_each(
			list.begin(),
			list.end(),
			[] (AST *node) {
				node->delete_tree();
			}
		);
	}
};

TEST_F(ParserStatementListTest, EndWithSemiColon)
{
	std::string text = "foo := 42; bar := 2 * 78 + 2;";
	Lexer lexer(text);
	Parser parser(&lexer);

	list = parser.statement_list();
	EXPECT_EQ((unsigned long)3, list.size());
}

TEST_F(ParserStatementListTest, EndWithoutSemiColon)
{
	std::string text = "foo := 42; bar := 2 * 78 + 2";
	Lexer lexer(text);
	Parser parser(&lexer);

	list = parser.statement_list();
	EXPECT_EQ((unsigned long)2, list.size());
}

TEST_F(ParserStatementListTest, Invalid)
{
	std::string text = "foo := 42; bar := 2 * 78 + 2; toto";
	Lexer lexer(text);
	Parser parser(&lexer);

	try
	{
		list = parser.statement_list();
		FAIL();
	}
	catch (const char *err)
	{
		EXPECT_STREQ("invalid syntax", err);
	}
}

TEST_F(ParserTest, ProgramValid)
{
	std::string text = "BEGIN\
		foo := 42; bar := 2 * 78 + 2;\
		END.";
	Lexer lexer(text);
	Parser parser(&lexer);

	node = parser.program();
	SUCCEED();
}

TEST_F(ParserTest, ProgramInvalid)
{
	std::string text = "BEGIN\
		foo := 42; bar := 2 * 78 + 2;\
		END";
	Lexer lexer(text);
	Parser parser(&lexer);

	try
	{
		node = parser.program();
	}
	catch (const char *err)
	{
		EXPECT_STREQ("invalid syntax", err);
		node = nullptr;
	}
}
